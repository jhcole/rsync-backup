#!/bin/bash
# Usage: rsync-backup.sh <src> <dst> <label>
if [ "$#" -ne 3 ]; then
    echo "$0: Expected 3 arguments, received $#: $@" >&2
    exit 1
fi
SSID=`iwgetid -r`
if [ "$SSID" = "MIFI2-97TS" ]; then
  echo "backup aborted to save data on MIFI device"
  exit 1
fi

HOST="Nikola"
TMPLOG="/tmp/rsync-backup.log"
EXCLUDE="/usr/local/rsync-homedir-excludes.txt"
SYNCARGS=" -avh"
SYNCARGS+=" --hard-links"
SYNCARGS+=" --sparse"
SYNCARGS+=" --fuzzy"
SYNCARGS+=" --partial"
SYNCARGS+=" --progress"
SYNCARGS+=" --log-file=$TMPLOG"
SYNCARGS+=" --exclude-from=$EXCLUDE"
if ( ssh $HOST "[ -d $2/__prev/ ]" ); then
  SYNCARGS+=" --delete"
  SYNCARGS+=" --delete-excluded"
  SYNCARGS+=" --link-dest=../../__prev/"
fi

rsync $SYNCARGS "$1" "$HOST:$2/$3"
ssh $HOST "rm -f $2/__prev && ln -s $3 $2/__prev"
scp $TMPLOG "$HOST:$2/$3/"
rm $TMPLOG
