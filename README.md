# rsync-backup

## Installation:

1. Modify the exclude list as desired then copy it to `/usr/local`.

2. Modify the rsync-backup.sh script if desired and copy to `/usr/local/sbin`.

3. Make sure root can ssh into the remote host without a password.

    1. Generate a new key with:

            $ sudo ssh-keygen -t rsa -b 4096

    2. Append the generated .pub to the `authorized_keys` file of the remote host user.

    3. Add the host information to `/root/.ssh/config`

            Host Nikola
              Port 22
              User username
              Hostname 192.168.0.1

4. Add cron tasks to the root crontab to run the script as desired. The following cron jobs create hourly, daily, and monthly backups

        # First day of the next month -> persistent
        0 0 1 * * /usr/local/sbin/rsync-backup.sh /home/user/ /nfs/sdata/Backup/User/Machine `date --date=yesterday +monthly/\%Y-\%m-\%d`
        # Other days of the month -> recycled next month
        0 0 2-31 * * /usr/local/sbin/rsync-backup.sh /home/user/ /nfs/sdata/Backup/User/Machine `date --date=yesterday +daily/\%d`
        # Other hours of day -> recycled next day
        0 1-23 * * * /usr/local/sbin/rsync-backup.sh /home/user/ /nfs/sdata/Backup/User/Machine `date --date="5 minutes ago" +hourly/\%H`
